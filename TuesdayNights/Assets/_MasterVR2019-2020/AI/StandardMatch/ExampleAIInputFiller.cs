﻿using UnityEngine;
using TuesdayNights;

public class ExampleAIInputFiller : tnStandardAIInputFillerBase
{
    private AIRole m_Role = AIRole.Null;

    private float m_AttractRadius = 0.85f;
    private float m_AttractMinRadius = 0.70f;
    private float m_AttractMaxRadius = 0.95f;

    //private float m_LowDistanceOfBallFromGoal = 4f;
    private float m_HighDistanceFromBall = 3.5f;

    public float m_HorizontalAxis = 0f;
    public float m_VerticalAxis = 0f;
    public bool m_PassButton = false;
    public bool m_ShotButton = false;
    public bool m_AttractButton = false;



    // Seek-and-flee behaviour.

    private float m_MinFleeDistanceFactor = 0.25f;
    private float m_MaxFleeDistanceFactor = 0.50f;

    private float m_MinAttractEnergy = 0.10f;

    // Separation.

    private float m_SeparationThreshold = 3f;

    // tnInputFiller's INTERFACE

    public override void Fill(float i_FrameTime, tnInputData i_Data)
    {
        if (!initialized || self == null)
        {
            ResetInputData(i_Data);
            return;
        }

        UpdateRecoverTimer(i_FrameTime);

        if (GetTeammateNearestTo(ball, false) == null)
        {
            IASolo();
        }
        else
        {
            IATeam();
        }

        if (m_RecoverTimer > m_RecoverTimeThreshold)
        {
            m_PassButton = true; // Force kick to recover.
        }

        //Fill input data

        i_Data.SetAxis(InputActions.s_HorizontalAxis, m_HorizontalAxis);
        i_Data.SetAxis(InputActions.s_VerticalAxis, m_VerticalAxis);

        i_Data.SetButton(InputActions.s_PassButton, m_PassButton);
        i_Data.SetButton(InputActions.s_ShotButton, m_ShotButton);
        i_Data.SetButton(InputActions.s_AttractButton, m_AttractButton);

    }

    private float m_RecoverRadius = 1.0f;
    private float m_RecoverTimeThreshold = 1.0f;
    private float m_RecoverTimer = 0f;

    private void UpdateRecoverTimer(float i_FrameTime)
    {
        float ballSpeed = GetVehicleSpeed(ball);
        float mySpeed = GetVehicleSpeed(self);

        if (ballDistance < m_RecoverRadius && (ballSpeed < 0.15f && mySpeed < 0.15f))
        {
            m_RecoverTimer += i_FrameTime;
        }
        else
        {
            m_RecoverTimer = 0f;
        }
    }
    private Vector2 ClampPosition(Vector2 i_Position, float i_Tolerance = 0f)
    {
        // Check left.

        float leftThreshold = midfield.x - halfFieldWidth;
        leftThreshold += i_Tolerance;

        if (i_Position.x < leftThreshold)
        {
            i_Position.x = leftThreshold;
        }

        // Check right.

        float rightThreshold = midfield.x + halfFieldWidth;
        rightThreshold -= i_Tolerance;

        if (i_Position.x > rightThreshold)
        {
            i_Position.x = rightThreshold;
        }

        // Check top.

        float topThreshold = midfield.y + halfFieldHeight;
        topThreshold -= i_Tolerance;

        if (i_Position.y > topThreshold)
        {
            i_Position.y = topThreshold;
        }

        // Check bottom.

        float bottomThreshold = midfield.y - halfFieldHeight;
        bottomThreshold += i_Tolerance;

        if (i_Position.y < bottomThreshold)
        {
            i_Position.y = bottomThreshold;
        }

        return i_Position;
    }

    public void IASolo() 
    {
        m_HorizontalAxis = 0f;
        m_VerticalAxis = 0f;
        m_PassButton = false;
        m_ShotButton = false;
        m_AttractButton = false;

        float m_LowDistanceOfBallFromGoal = fieldWidth / 10f * 2f;
        
        //Determina se sei tu o un altro nemico ad essere il più vicino alla palla
        if (Vector2.Distance(GetOpponentNearestTo(ball).position, ballPosition) >= ballDistance)
        {
            //Sei più vicino alla palla rispetto all'avversario più vicino
            if (ballDistance < m_AttractRadius) //Se sei nel raggio dell'attract
            {

                //Controlla la palla, mantenendola nella direzione giusta 
                //e calciandola, qualora ne avessi la possibilità

                Vector2 axes;
                bool attractNeeded;
                bool kickNeeded;

                BallControl(out axes, out attractNeeded, out kickNeeded, false);

                m_AttractButton = attractNeeded;
                m_PassButton = kickNeeded;
                m_HorizontalAxis = axes.x;
                m_VerticalAxis = axes.y;

                //Vector2 direction;
                //DirectionToAim(out direction, false);

                //Vector2 target;
                //SetTargetDirection(direction, out target);

                //Vector2 axes;
                //if (UpdateAttract(target, out axes)) //Sto attraendo?
                //{
                //    //Sì, quindi aggiusto la mia posizione
                //    m_HorizontalAxis = axes.x;
                //    m_VerticalAxis = axes.y;

                //    m_AttractButton = true;
                //}
                //else
                //{
                //    //Non sto attraendo

                //    m_VerticalAxis = Seek(target).y;
                //    m_HorizontalAxis = Seek(target).x;

                //}

                //if (Vector2.Distance(myPosition, target) <= 0.2f)
                //{
                //    //La palla è nella direzione giusta, calcio
                //    m_AttractButton = false;
                //    m_PassButton = true;
                //}
            }
            else //Non sono nel raggio dell'attract
            {
                ////Raggiungo la palla
                //Vector2 direction = Seek(ball);
                //m_HorizontalAxis = direction.x;
                //m_VerticalAxis = direction.y;

                ////Decido se la distanza tra me e la palla è abbastanza alta per dashare
                //if (ballDistance >= m_HighDistanceFromBall - 0.3f)
                //{
                //    m_ShotButton = true;
                //}

                Vector2 axes;
                bool dashNeeded;

                ReachTheBallSolo(out axes, out dashNeeded);

                m_ShotButton = dashNeeded;
                m_HorizontalAxis = axes.x;
                m_VerticalAxis = axes.y;
            }
        }
        else //Il più vicino alla palla è l'avversario
        {
            if (Vector2.Distance(ballPosition, myGoalPosition) <= 7f && Vector2.Distance(ballPosition, myGoalPosition) >= 1f) //La palla si sta avvicinando molto alla zona della mia porta
            {
                //Decido il peso dell'interpose a seconda dalla distanza della palla dalla porta
                float weight = 0.65f;
                if (Vector2.Distance(ballPosition, myGoalPosition) <= 4.5f)
                    weight = 0.45f;

                //Interpose tra palla e la mia porta, vicino alla porta
                m_HorizontalAxis = Interpose(ball, myGoal, weight).x;
                m_VerticalAxis = Interpose(ball, myGoal, weight).y;

                //Decido se la distanza è tale da poter dashare per fare l'interpose
                if (Vector2.Distance(Vector2.Lerp(ballPosition, myGoalPosition, weight), myPosition) > m_HighDistanceFromBall - 0.5f)
                {
                    m_ShotButton = true;
                }

            }
            else //La palla non è vicino alla mia porta
            {
                //Interpose tra palla e la nostra porta, vicino alla palla
                m_HorizontalAxis = Interpose(ball, myGoal, 0.025f).x;
                m_VerticalAxis = Interpose(ball, myGoal, 0.025f).y;

                //Decido se è necessario dashare per fare l'interpose
                if (Vector2.Distance(ballPosition, myPosition) > m_HighDistanceFromBall - 0.4f)
                {
                    m_ShotButton = true;
                }
            }

            if (Vector2.Distance(myPosition,myGoalPosition) >= Vector2.Distance(GetOpponentNearestTo(ball).position.GetVector2XY(), myGoalPosition) //sono più distante dell'avversario dalla mia porta
                && Vector2.Distance(GetOpponentNearestTo(ball).position.GetVector2XY(), myGoalPosition) >= Vector2.Distance(ballPosition, myGoalPosition)) //l'avversario è più distante della palla dalla mia porta
            {
                //Recupero la palla
                Vector2 axes;
                bool dashNeeded;
                Recover(myGoalPosition, out axes, out dashNeeded);
                m_HorizontalAxis = axes.x;
                m_VerticalAxis = axes.y;
                m_ShotButton = dashNeeded;
            }
        }
        

    }

    public void IATeam()
    {
        m_HorizontalAxis = 0f;
        m_VerticalAxis = 0f;
        m_PassButton = false;
        m_ShotButton = false;
        m_AttractButton = false;

        float m_LowDistanceOfBallFromGoal = fieldWidth / 10f * 2f;


        //Determina se sei tu o un altro alleato ad essere il più vicino alla palla
        if (Vector2.Distance(GetTeammateNearestTo(ball, false).position, ballPosition) >= ballDistance) //Il più vicino alla palla degli alleati sei tu
        {
            //Sono il più vicino della mia squadra alla palla
                
            if (ballDistance < m_AttractRadius) //Se sei nel raggio dell'attract     
            {
                //Controlla la palla, mantenendola nella direzione giusta 
                //e calciandola, qualora ne avessi la possibilità

                Vector2 axes;
                bool attractNeeded;
                bool kickNeeded;

                BallControl(out axes, out attractNeeded, out kickNeeded);

                m_AttractButton = attractNeeded;
                m_PassButton = kickNeeded;
                m_HorizontalAxis = axes.x;
                m_VerticalAxis = axes.y;

            }
            else //Non sono nel raggio dell'attract
            {
                //Raggiungi la palla    

                Vector2 axes;
                bool dashNeeded;

                ReachTheBall(out axes, out dashNeeded);

                m_ShotButton = dashNeeded;
                m_HorizontalAxis = axes.x;
                m_VerticalAxis = axes.y;
            }
        }
        else //Il più vicino alla palla degli alleati non sei tu
        {
            if (m_Role == AIRole.Striker) //Se sei un attaccante
            {
                // Avanza in attacco spostandoti di lato rispetto alla palla,
                // per intercettare la palla senza intralciarne il possessore 

                Vector2 axes;

                ComputeStrikerPatrolPosition(out axes);

                m_HorizontalAxis = axes.x;
                m_VerticalAxis = axes.y;

            }
            else //Sei un difensore o un centrocampista
            {
                if (GetTeammateNearestTo(myGoal, true) == GetTeammateNearestTo(myGoal, false) //Non sono il più vicino alla mia porta
                    && GetTeammateNearestTo(myGoal, false) != GetTeammateNearestTo(ball, false)) //Il portiere non ha la palla (in 3v3 ce l'ha l'attaccante)
                {
                    // Indietreggia leggermente in difesa di lato rispetto alla palla, 
                    // per intercettare la palla qualora venisse calciata in questa direzione 

                    Vector2 axes;

                    ComputeDefenderPatrolPosition(out axes);

                    m_HorizontalAxis = axes.x;
                    m_VerticalAxis = axes.y;
                }
                else //Sono il più vicino alla mia porta oppure il portiere ha la palla
                {
                    Vector2 defencePosition;
                    ComputeRelativeToGoalPosition(out defencePosition);

                    if (Vector2.Distance(ballPosition, myGoalPosition) < m_LowDistanceOfBallFromGoal) //Se la palla è a ridosso della mia porta
                    {   
                        // La palla si trova a ridosso della mia porta
                        // Dasho in maniera più aggressiva del solito verso la mia porta, o rischio di subire un goal!

                        Vector2 axes;
                        bool dashNeeded;
                        Recover(myPosition + Interpose(ballPosition, defencePosition, 0.3f), out axes, out dashNeeded);
                        m_HorizontalAxis = axes.x;
                        m_VerticalAxis = axes.y;
                        m_ShotButton = dashNeeded;

                    }
                    else
                    {
                        // Mi posiziono fra la palla e la mia porta

                        m_HorizontalAxis = Interpose(ballPosition, defencePosition).x;
                        m_VerticalAxis = Interpose(ballPosition, defencePosition).y;

                        //Se sono troppo distante dasho in difesa
                        if (Vector2.Distance(Vector2.Lerp(ballPosition, defencePosition, 0.5f), myPosition) >= m_HighDistanceFromBall)
                        {
                            m_ShotButton = true;
                        }
                    }

                    //Se ho la palla a portata di calcio, ed è nella direzione giusta, la calcio, anche se non ne ho il possesso

                    Vector2 direction;
                    DirectionToAim(out direction, false);
                    Vector2 target;
                    SetTargetDirection(direction, out target);

                    if (Vector2.Distance(myPosition, target) < 0.25f)
                    {
                        m_PassButton = true;
                    }

                }
            }
        }
    }

    public override void Clear(){}

    public ExampleAIInputFiller(GameObject i_Self, AIRole i_Role) : base(i_Self)
    {
        m_Role = i_Role;
    }

    /// <summary>
    /// Set target direction to aim, which can be the opposing goal or teammates closer to opposing door
    /// </summary>
    /// <param name="direction">- The direction to aim</param>
    /// <param name="includeTeammates">- Wheter or not to include teammates</param>
    private void DirectionToAim(out Vector2 direction, bool includeTeammates = true)
    {
        if (includeTeammates)
        {
            Transform teammate = GetTeammateNearestTo(opponentGoal, false);
            Vector2 teammatePosition = teammate.position;

            float myDistance = Vector2.Distance(myPosition, opponentGoalPosition);
            float teammateDistance = Vector2.Distance(teammatePosition, opponentGoalPosition);

            if (myDistance > teammateDistance + fieldWidth / 10.0f)  // <= uno scarto accettabile, per evitare passaggi troppo verticali 
            {
                //Se l'alleato è più vicino alla porta, miro verso di lui

                Vector2 ballToTeammateDirection = opponentGoalPosition - ballPosition;
                Vector2 teammateToGoalDirection = teammatePosition - ballPosition;
                Vector2 resultingDirection = teammateToGoalDirection + ballToTeammateDirection;
                direction = ballPosition - resultingDirection;
            }
            else
            {
                //Altrimenti miro verso la porta avversaria
                direction = ballPosition - opponentGoalPosition;
            }
        }
        else
        {
            direction = ballPosition - opponentGoalPosition;
        }

        direction.Normalize();
    }

    /// <summary>
    /// Given the direction, it will set the relative target direction to the ball position
    /// </summary>
    /// <param name="direction">- Direction to aim</param>
    /// <param name="target">- Target direction relative to the ball</param>
    private void SetTargetDirection(in Vector2 direction, out Vector2 target)
    {
        target = ballPosition;
        float offset = ballRadius;
        offset += colliderRadius;
        offset += goalWidth / 20; // Tolerance threshold.


        target += direction * offset;

        target = ClampPosition(target, 0.2f);
    }

    /// <summary>
    /// Set axes nexessary to reach the ball, specifying if dash is needed
    /// </summary>
    private void ReachTheBall(out Vector2 axes, out bool dash)
    {
        dash = false;
        axes = Seek(ball, ballRadius);
        
        if (ballDistance >= m_HighDistanceFromBall)
        {
            dash = true;
        }
    }

    /// <summary>
    /// Set axes necessary to keep control of the ball, specifying if dash or kick are needed
    /// </summary>
    private void BallControl(out Vector2 axes, out bool attractNeeded, out bool kickNeeded, bool aimForTeamatesToo = true) 
    {
        attractNeeded = false;
        kickNeeded = false;

        Vector2 direction;
        DirectionToAim(out direction, aimForTeamatesToo);

        Vector2 target;
        SetTargetDirection(direction, out target);

        if (UpdateAttract(target, out axes)) //Sto attraendo?
        {
            attractNeeded = true;
        }
        else //Non sto attraendo
        {
            axes = Seek(target);
        }

        if (Vector2.Distance(myPosition, target) < 0.2f)
        {
            //La palla è nella direzione giusta e a portata -> calcio
            kickNeeded = true;
        }
    }

    /// <summary>
    /// Set axes needed to reach the position for striker when not possessing the ball
    /// </summary>
    private void ComputeStrikerPatrolPosition(out Vector2 axes) 
    {
        float yCoord;
        if (ballPosition.y < 0)
            yCoord = ballPosition.y + halfFieldHeight / 2;
        else
            yCoord = ballPosition.y - halfFieldHeight / 2;

        float xCoord;
        if (IsBallInMyHalfSide()) //Se la palla è nella mia metà campo
            xCoord = Mathf.Lerp(ballPosition.x, opponentGoalPosition.x, 0.1f);
            //xCoord = Vector2.Lerp(ballPosition, opponentGoalPosition, 0.1f).x;
        else //Altrimenti
            xCoord = Mathf.Lerp(ballPosition.x, opponentGoalPosition.x, 0.2f);
            //xCoord = Vector2.Lerp(ballPosition, opponentGoalPosition, 0.2f).x;

        Vector2 newPosition = new Vector2(xCoord, yCoord);
        Vector2 destination = newPosition - myPosition;

        axes = Seek(destination);

    }

    /// <summary>
    /// Set axes needed to reach the position for defender when not possessing the ball
    /// </summary>
    private void ComputeDefenderPatrolPosition(out Vector2 axes)
    {
        float yCoord;
        if (ballPosition.y < 0)
            yCoord = ballPosition.y + halfFieldHeight / 2;
        else
            yCoord = ballPosition.y - halfFieldHeight / 2;

        float xCoord;
        if (IsBallInMyHalfSide()) //Se la palla è nella mia metà campo
            xCoord = Mathf.Lerp(ballPosition.x, myGoalPosition.x, 0.2f);
            //xCoord = Vector2.Lerp(ballPosition, myGoalPosition, 0.2f).x;
        else //Altrimenti
            xCoord = Mathf.Lerp(ballPosition.x, myGoalPosition.x, 0.1f);
            //xCoord = Vector2.Lerp(ballPosition, myGoalPosition, 0.1f).x;

        Vector2 newPosition = new Vector2(xCoord, yCoord);
        Vector2 destination = newPosition - myPosition;

        axes = Seek(destination);

    }

    /// <summary>
    /// Set axes needed to reach the relative-to-goal position for the goalkeeper
    /// </summary>
    private void ComputeRelativeToGoalPosition(out Vector2 destination) 
    {
        destination = myGoalPosition;

        Vector2 ballVelocity = GetVehicleVelocity(ball);

        if ( (myGoalPosition.x < 0 && ballVelocity.x < 0)   // La mia porta è a sinistra e la palla sta andando a sinistra
            || (myGoalPosition.x > 0 && ballVelocity.x > 0)) // La mia porta è a destra e la palla sta andando a destra
        {
            Vector2 ballCurrentDirection = ballVelocity / ballVelocity.x;
            float horizontalDistance = Mathf.Abs(myGoalPosition.x - ballPosition.x);
            Vector2 relativeBallPosition = ballCurrentDirection * horizontalDistance;
            Vector2 relativeToGoalDestination = ballPosition + relativeBallPosition;

            if(relativeToGoalDestination.y >= (myGoalPosition.y - (goalWidth / 2))
                && relativeToGoalDestination.y <= (myGoalPosition.y + (goalWidth / 2)))
            {
                destination = relativeToGoalDestination;
            }
        }
    }

    private Vector2 Interpose (Vector2 a, Vector2 b, float weight = 0.5f) 
    {
        weight = Mathf.Clamp(weight, 0.0f, 1.0f);

        Vector2 midPoint = Vector2.Lerp(a,b,weight);

        return Seek(midPoint);
    }


    private void Recover(Vector2 i_Target, out Vector2 o_Axes, out bool o_DashButton)
    {
        Vector2 targetDistance = i_Target - myPosition;

        Vector2 targetDirection = targetDistance.normalized;

        {
            // Apply direction correction in order to avoid the ball.

            Vector2 pointA = ballPosition;
            pointA.y += ballRadius;
            pointA.y += colliderRadius;
            pointA.y += 0.05f; // Safety tolerance.

            Vector2 pointB = ballPosition;
            pointB.y -= ballRadius;
            pointB.y -= colliderRadius;
            pointB.y -= 0.05f; // Safety tolerance.

            Vector2 selfToA = pointA - myPosition;
            Vector2 selfToB = pointB - myPosition;

            Vector2 directionA = selfToA.normalized;
            Vector2 directionB = selfToB.normalized;

            float angleA = Vector2.Angle(targetDirection, directionA);
            float angleB = Vector2.Angle(targetDirection, directionB);

            float angleAB = Vector2.Angle(directionA, directionB);

            if (angleA < angleAB && angleB < angleAB)
            {
                // Target direction is into ball fov. Apply a correction using the shortest line.

                if (selfToA.sqrMagnitude < selfToB.sqrMagnitude) // A is the nearest point.
                {
                    targetDirection = directionA;
                }
                else // B is the nearest point.
                {
                    targetDirection = directionB;
                }
            }
            else
            {
                // Target direction is fine. Nothing to do.
            }
        }

        o_Axes = targetDirection.normalized;

        if (targetDistance.sqrMagnitude > 4f)
        {
            // Force dash movement, to recover your position quickly.

            o_DashButton = true;
        }
        else
        {
            o_DashButton = false;
        }
    }

    private bool UpdateAttract(Vector2 target, out Vector2 o_Axes)
    {
        o_Axes = Vector2.zero;

        bool isAttracting = false;

        if (CheckEnergy(m_MinAttractEnergy))
        {
             if (!IsBehindBall(self) && ballDistance < m_AttractMaxRadius)
             {
                if (ballDistance < m_AttractMinRadius)
                {
                    Vector2 attractDirection = Vector2.zero;
                    bool forcedDirection = false;

                    if (myGoalPosition.x < midfield.x)
                    {
                        if (!IsCloseToRightBorder(ball, 0.1f))
                        {
                            if (IsCloseToLeftBorder(ball, 0.1f))
                            {
                                attractDirection += Vector2.right;
                                forcedDirection = true;
                            }

                            if (IsCloseToTopBorder(ball, 0.1f))
                            {
                                attractDirection += Vector2.down;
                                forcedDirection = true;
                            }

                            if (IsCloseToBottomBorder(ball, 0.1f))
                            {
                                attractDirection += Vector2.up;
                                forcedDirection = true;
                            }
                        }
                    }

                    if (myGoalPosition.x > midfield.x)
                    {
                        if (!IsCloseToLeftBorder(ball, 0.1f))
                        {
                            if (IsCloseToRightBorder(ball, 0.1f))
                            {
                                attractDirection += Vector2.left;
                                forcedDirection = true;
                            }

                            if (IsCloseToTopBorder(ball, 0.1f))
                            {
                                attractDirection += Vector2.down;
                                forcedDirection = true;
                            }

                            if (IsCloseToBottomBorder(ball, 0.1f))
                            {
                                attractDirection += Vector2.up;
                                forcedDirection = true;
                            }
                        }
                    }

                    if (!forcedDirection)
                    {
                        Vector2 toShotDirection = target - myPosition;

                        Vector2 ballDirection = GetBallDirection(self);
                        Vector2 axisA = new Vector2(ballDirection.y, -ballDirection.x);
                        Vector2 axisB = -axisA;

                        float dotA = Vector2.Dot(axisA, toShotDirection);
                        float dotB = Vector2.Dot(axisB, toShotDirection);

                        if (dotA > dotB)
                        {
                            attractDirection = axisA;
                        }
                        else // dotB > dotA
                        {
                            attractDirection = axisB;
                        }
                    }

                    o_Axes = attractDirection; // The ball is very close to you, you can start moving with it attached.
                }
                else
                {
                    o_Axes = Vector2.zero;
                }

                isAttracting = true;
             }
            
        }

        return isAttracting;
    }

    private void ReachTheBallSolo(out Vector2 axes, out bool dash)
    {
        dash = false;
        axes = Seek(ball, ballRadius); //Raggiungo la palla

        if (ballDistance >= m_HighDistanceFromBall -0.3f) //Decido se la distanza tra me e la palla è abbastanza alta per dashare
        {
            dash = true;
        }
    }
}

